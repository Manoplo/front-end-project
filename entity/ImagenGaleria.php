<?php

class ImagenGaleria
{
    private $nombre;
    private $descripcion;


    private $numVisualizaciones;
    private $numLikes;
    private $numDownloads;

    const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALLERY = "images/index/gallery/";


    public function __construct($nombre, $descripcion, $numVisualizaciones, $numLikes, $numDownloads)

    {
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
    }



    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     */
    public function setNombre($nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     */
    public function setDescripcion($descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of numVisualizaciones
     */
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * Set the value of numVisualizaciones
     */
    public function setNumVisualizaciones($numVisualizaciones): self
    {
        $this->numVisualizaciones = $numVisualizaciones;

        return $this;
    }

    /**
     * Get the value of numLikes
     */
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Set the value of numLikes
     */
    public function setNumLikes($numLikes): self
    {
        $this->numLikes = $numLikes;

        return $this;
    }

    /**
     * Get the value of numDownloads
     */
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
     * Set the value of numDownloads
     */
    public function setNumDownloads($numDownloads): self
    {
        $this->numDownloads = $numDownloads;

        return $this;
    }

    /*  Create a function to pass parameters when creating an object to string */

    public function __toString()
    {
        return $this->getDescripcion();
    }

    /*
    Create a function which will return the exact URL of an specific image in portfolio folder
     */

    public function getURLPortfolio(): string
    {

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    /*
    Create a function which will return the exact URL of an specific image in portfolio folder
     */

    public function getURLGallery(): string
    {

        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }
}
