<?php

if(htmlspecialchars($_SERVER['REQUEST_METHOD'] === 'POST')){

      // Recibimos los inputs. 

    $data  = array(
        "fname" => htmlspecialchars($_POST["fname"]) ?? null,
        "lname" => htmlspecialchars($_POST["lname"]) ?? null,
        "email" => htmlspecialchars($_POST["email"]) ?? null,
        "subject" => htmlspecialchars($_POST["subject"]) ?? null,
        "message" =>  htmlspecialchars($_POST["message"]) ?? null
    );


    // Array vacío para capturar los campos obligatorios
    $errors = array();


    // Lógica del programa

    if(empty($data['fname'])){

        $errors["fname"] = "First name is required";

    }
    
    if(empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL) ){

        $errors["email"] = "Email is required or invalid email adress";

    }
    
    if(empty($data['subject'])){

        $errors["subject"] = "Subject is required";

    } 

    

}
include "utils/utils.php";

require "views/contact.view.php";