<?php
include "utils/utils.php";
require_once "models/Partner.php";

require "entity/ImagenGaleria.php";

$images = array();
$partners = [
    [
        "name" => "Google",
        "logo" => "https://e7.pngegg.com/pngimages/760/624/png-clipart-google-logo-google-search-advertising-google-company-text-thumbnail.png",
        "description" => "Google Partner"
    ],
    [
        "name" => "Apple",
        "logo" => "https://www.rightscon.org/cms/assets/uploads/2021/05/RC21-Apple-logo-small.png",
        "description" => "Apple Partner"
    ],
    [
        "name" => "Microsoft",
        "logo" => "https://www.masgamers.com/wp-content/uploads/2015/11/Microsoft-logo-m-box-880x660.png",
        "description" => "Microsoft Partner"

    ],
    [
        "name" => "Facebook",
        "logo" => "https://i0.wp.com/cityscoop.com.ng/wp-content/uploads/2018/04/IMG_1680.png?fit=280%2C280&ssl=1",
        "description" => "Facebook Partner"
    ],
    [
        "name" => "Twitter",
        "logo" => "https://cpng.pikpng.com/pngl/s/31-312988_small-twitter-logo-dark-blue-clipart.png",
        "description" => "Twitter Partner"
    ]
];

for ($i = 1; $i <= 12; $i++) {
    $randomViews = rand(0, 1500);
    $randomLikes = rand(0, 1500);
    $randomDownloads = rand(0, 1500);

    $images[$i] = new ImagenGaleria("$i.jpg", "descripción imagen $i", $randomViews, $randomLikes, $randomDownloads);
}

$nPartners = shuffleAndGet($partners);


$objPartners = array();

for ($i = 0; $i < count($nPartners); $i++) {

    $objPartners[$i] = new Partner($nPartners[$i]['name'], $nPartners[$i]['logo'], $nPartners[$i]['description']);
}

/* print_r($objPartners); */

require "views/index.view.php";
