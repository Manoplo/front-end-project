<?php


function isActive($string)
{

    if (strpos($_SERVER['REQUEST_URI'], $string)) {

        return true;
    } else {

        return false;
    }
}

function shuffleAndGet($array)
{

    shuffle($array);

    if (count($array) <= 3) {

        return $array;
    } else {

        $newArray = array_slice($array, 0, 3);
        return $newArray;
    }
}
